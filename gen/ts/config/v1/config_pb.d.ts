// package: kcs.config.v1
// file: config/v1/config.proto

import * as jspb from "google-protobuf";

export class Config extends jspb.Message {
  getKind(): KindMap[keyof KindMap];
  setKind(value: KindMap[keyof KindMap]): void;

  getName(): string;
  setName(value: string): void;

  getFormat(): FormatMap[keyof FormatMap];
  setFormat(value: FormatMap[keyof FormatMap]): void;

  getNamespace(): string;
  setNamespace(value: string): void;

  getPayload(): Uint8Array | string;
  getPayload_asU8(): Uint8Array;
  getPayload_asB64(): string;
  setPayload(value: Uint8Array | string): void;

  getKvPairsMap(): jspb.Map<string, string>;
  clearKvPairsMap(): void;
  getActive(): boolean;
  setActive(value: boolean): void;

  getActiveSince(): number;
  setActiveSince(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Config.AsObject;
  static toObject(includeInstance: boolean, msg: Config): Config.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Config, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Config;
  static deserializeBinaryFromReader(message: Config, reader: jspb.BinaryReader): Config;
}

export namespace Config {
  export type AsObject = {
    kind: KindMap[keyof KindMap],
    name: string,
    format: FormatMap[keyof FormatMap],
    namespace: string,
    payload: Uint8Array | string,
    kvPairsMap: Array<[string, string]>,
    active: boolean,
    activeSince: number,
  }
}

export interface FormatMap {
  JSON: 0;
  YAML: 1;
  HCL: 2;
  FILE: 3;
  LITERAL: 4;
  KV: 5;
}

export const Format: FormatMap;

export interface KindMap {
  PLAIN_TEXT: 0;
  SECRET: 1;
}

export const Kind: KindMap;

