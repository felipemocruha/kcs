// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: config/v1/config_api.proto

package v1

import (
	context "context"
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion2 // please upgrade the proto package

type ListConfigsRequest struct {
	OnlyActive           bool     `protobuf:"varint,1,opt,name=only_active,json=onlyActive,proto3" json:"only_active,omitempty"`
	Namespace            string   `protobuf:"bytes,2,opt,name=namespace,proto3" json:"namespace,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ListConfigsRequest) Reset()         { *m = ListConfigsRequest{} }
func (m *ListConfigsRequest) String() string { return proto.CompactTextString(m) }
func (*ListConfigsRequest) ProtoMessage()    {}
func (*ListConfigsRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f87cee1f80c154c0, []int{0}
}
func (m *ListConfigsRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListConfigsRequest.Unmarshal(m, b)
}
func (m *ListConfigsRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListConfigsRequest.Marshal(b, m, deterministic)
}
func (m *ListConfigsRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListConfigsRequest.Merge(m, src)
}
func (m *ListConfigsRequest) XXX_Size() int {
	return xxx_messageInfo_ListConfigsRequest.Size(m)
}
func (m *ListConfigsRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ListConfigsRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ListConfigsRequest proto.InternalMessageInfo

func (m *ListConfigsRequest) GetOnlyActive() bool {
	if m != nil {
		return m.OnlyActive
	}
	return false
}

func (m *ListConfigsRequest) GetNamespace() string {
	if m != nil {
		return m.Namespace
	}
	return ""
}

type ListConfigsResponse struct {
	ConfigsFound         int32     `protobuf:"varint,1,opt,name=configs_found,json=configsFound,proto3" json:"configs_found,omitempty"`
	ConfigsActive        int32     `protobuf:"varint,2,opt,name=configs_active,json=configsActive,proto3" json:"configs_active,omitempty"`
	Configs              []*Config `protobuf:"bytes,3,rep,name=configs,proto3" json:"configs,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *ListConfigsResponse) Reset()         { *m = ListConfigsResponse{} }
func (m *ListConfigsResponse) String() string { return proto.CompactTextString(m) }
func (*ListConfigsResponse) ProtoMessage()    {}
func (*ListConfigsResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f87cee1f80c154c0, []int{1}
}
func (m *ListConfigsResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListConfigsResponse.Unmarshal(m, b)
}
func (m *ListConfigsResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListConfigsResponse.Marshal(b, m, deterministic)
}
func (m *ListConfigsResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListConfigsResponse.Merge(m, src)
}
func (m *ListConfigsResponse) XXX_Size() int {
	return xxx_messageInfo_ListConfigsResponse.Size(m)
}
func (m *ListConfigsResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ListConfigsResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ListConfigsResponse proto.InternalMessageInfo

func (m *ListConfigsResponse) GetConfigsFound() int32 {
	if m != nil {
		return m.ConfigsFound
	}
	return 0
}

func (m *ListConfigsResponse) GetConfigsActive() int32 {
	if m != nil {
		return m.ConfigsActive
	}
	return 0
}

func (m *ListConfigsResponse) GetConfigs() []*Config {
	if m != nil {
		return m.Configs
	}
	return nil
}

type GetConfigRequest struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Namespace            string   `protobuf:"bytes,2,opt,name=namespace,proto3" json:"namespace,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetConfigRequest) Reset()         { *m = GetConfigRequest{} }
func (m *GetConfigRequest) String() string { return proto.CompactTextString(m) }
func (*GetConfigRequest) ProtoMessage()    {}
func (*GetConfigRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f87cee1f80c154c0, []int{2}
}
func (m *GetConfigRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetConfigRequest.Unmarshal(m, b)
}
func (m *GetConfigRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetConfigRequest.Marshal(b, m, deterministic)
}
func (m *GetConfigRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetConfigRequest.Merge(m, src)
}
func (m *GetConfigRequest) XXX_Size() int {
	return xxx_messageInfo_GetConfigRequest.Size(m)
}
func (m *GetConfigRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetConfigRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetConfigRequest proto.InternalMessageInfo

func (m *GetConfigRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *GetConfigRequest) GetNamespace() string {
	if m != nil {
		return m.Namespace
	}
	return ""
}

type GetConfigResponse struct {
	Config               *Config  `protobuf:"bytes,1,opt,name=config,proto3" json:"config,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetConfigResponse) Reset()         { *m = GetConfigResponse{} }
func (m *GetConfigResponse) String() string { return proto.CompactTextString(m) }
func (*GetConfigResponse) ProtoMessage()    {}
func (*GetConfigResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f87cee1f80c154c0, []int{3}
}
func (m *GetConfigResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetConfigResponse.Unmarshal(m, b)
}
func (m *GetConfigResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetConfigResponse.Marshal(b, m, deterministic)
}
func (m *GetConfigResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetConfigResponse.Merge(m, src)
}
func (m *GetConfigResponse) XXX_Size() int {
	return xxx_messageInfo_GetConfigResponse.Size(m)
}
func (m *GetConfigResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetConfigResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetConfigResponse proto.InternalMessageInfo

func (m *GetConfigResponse) GetConfig() *Config {
	if m != nil {
		return m.Config
	}
	return nil
}

type CreateConfigRequest struct {
	Config               *Config  `protobuf:"bytes,1,opt,name=config,proto3" json:"config,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateConfigRequest) Reset()         { *m = CreateConfigRequest{} }
func (m *CreateConfigRequest) String() string { return proto.CompactTextString(m) }
func (*CreateConfigRequest) ProtoMessage()    {}
func (*CreateConfigRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f87cee1f80c154c0, []int{4}
}
func (m *CreateConfigRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateConfigRequest.Unmarshal(m, b)
}
func (m *CreateConfigRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateConfigRequest.Marshal(b, m, deterministic)
}
func (m *CreateConfigRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateConfigRequest.Merge(m, src)
}
func (m *CreateConfigRequest) XXX_Size() int {
	return xxx_messageInfo_CreateConfigRequest.Size(m)
}
func (m *CreateConfigRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateConfigRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreateConfigRequest proto.InternalMessageInfo

func (m *CreateConfigRequest) GetConfig() *Config {
	if m != nil {
		return m.Config
	}
	return nil
}

type CreateConfigResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateConfigResponse) Reset()         { *m = CreateConfigResponse{} }
func (m *CreateConfigResponse) String() string { return proto.CompactTextString(m) }
func (*CreateConfigResponse) ProtoMessage()    {}
func (*CreateConfigResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f87cee1f80c154c0, []int{5}
}
func (m *CreateConfigResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateConfigResponse.Unmarshal(m, b)
}
func (m *CreateConfigResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateConfigResponse.Marshal(b, m, deterministic)
}
func (m *CreateConfigResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateConfigResponse.Merge(m, src)
}
func (m *CreateConfigResponse) XXX_Size() int {
	return xxx_messageInfo_CreateConfigResponse.Size(m)
}
func (m *CreateConfigResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateConfigResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CreateConfigResponse proto.InternalMessageInfo

type DeleteConfigRequest struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Namespace            string   `protobuf:"bytes,2,opt,name=namespace,proto3" json:"namespace,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeleteConfigRequest) Reset()         { *m = DeleteConfigRequest{} }
func (m *DeleteConfigRequest) String() string { return proto.CompactTextString(m) }
func (*DeleteConfigRequest) ProtoMessage()    {}
func (*DeleteConfigRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f87cee1f80c154c0, []int{6}
}
func (m *DeleteConfigRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeleteConfigRequest.Unmarshal(m, b)
}
func (m *DeleteConfigRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeleteConfigRequest.Marshal(b, m, deterministic)
}
func (m *DeleteConfigRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeleteConfigRequest.Merge(m, src)
}
func (m *DeleteConfigRequest) XXX_Size() int {
	return xxx_messageInfo_DeleteConfigRequest.Size(m)
}
func (m *DeleteConfigRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_DeleteConfigRequest.DiscardUnknown(m)
}

var xxx_messageInfo_DeleteConfigRequest proto.InternalMessageInfo

func (m *DeleteConfigRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *DeleteConfigRequest) GetNamespace() string {
	if m != nil {
		return m.Namespace
	}
	return ""
}

type DeleteConfigResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeleteConfigResponse) Reset()         { *m = DeleteConfigResponse{} }
func (m *DeleteConfigResponse) String() string { return proto.CompactTextString(m) }
func (*DeleteConfigResponse) ProtoMessage()    {}
func (*DeleteConfigResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f87cee1f80c154c0, []int{7}
}
func (m *DeleteConfigResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeleteConfigResponse.Unmarshal(m, b)
}
func (m *DeleteConfigResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeleteConfigResponse.Marshal(b, m, deterministic)
}
func (m *DeleteConfigResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeleteConfigResponse.Merge(m, src)
}
func (m *DeleteConfigResponse) XXX_Size() int {
	return xxx_messageInfo_DeleteConfigResponse.Size(m)
}
func (m *DeleteConfigResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_DeleteConfigResponse.DiscardUnknown(m)
}

var xxx_messageInfo_DeleteConfigResponse proto.InternalMessageInfo

type DeactivateConfigRequest struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Namespace            string   `protobuf:"bytes,2,opt,name=namespace,proto3" json:"namespace,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeactivateConfigRequest) Reset()         { *m = DeactivateConfigRequest{} }
func (m *DeactivateConfigRequest) String() string { return proto.CompactTextString(m) }
func (*DeactivateConfigRequest) ProtoMessage()    {}
func (*DeactivateConfigRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f87cee1f80c154c0, []int{8}
}
func (m *DeactivateConfigRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeactivateConfigRequest.Unmarshal(m, b)
}
func (m *DeactivateConfigRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeactivateConfigRequest.Marshal(b, m, deterministic)
}
func (m *DeactivateConfigRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeactivateConfigRequest.Merge(m, src)
}
func (m *DeactivateConfigRequest) XXX_Size() int {
	return xxx_messageInfo_DeactivateConfigRequest.Size(m)
}
func (m *DeactivateConfigRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_DeactivateConfigRequest.DiscardUnknown(m)
}

var xxx_messageInfo_DeactivateConfigRequest proto.InternalMessageInfo

func (m *DeactivateConfigRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *DeactivateConfigRequest) GetNamespace() string {
	if m != nil {
		return m.Namespace
	}
	return ""
}

type DeactivateConfigResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeactivateConfigResponse) Reset()         { *m = DeactivateConfigResponse{} }
func (m *DeactivateConfigResponse) String() string { return proto.CompactTextString(m) }
func (*DeactivateConfigResponse) ProtoMessage()    {}
func (*DeactivateConfigResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f87cee1f80c154c0, []int{9}
}
func (m *DeactivateConfigResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeactivateConfigResponse.Unmarshal(m, b)
}
func (m *DeactivateConfigResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeactivateConfigResponse.Marshal(b, m, deterministic)
}
func (m *DeactivateConfigResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeactivateConfigResponse.Merge(m, src)
}
func (m *DeactivateConfigResponse) XXX_Size() int {
	return xxx_messageInfo_DeactivateConfigResponse.Size(m)
}
func (m *DeactivateConfigResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_DeactivateConfigResponse.DiscardUnknown(m)
}

var xxx_messageInfo_DeactivateConfigResponse proto.InternalMessageInfo

func init() {
	proto.RegisterType((*ListConfigsRequest)(nil), "kcs.config.v1.ListConfigsRequest")
	proto.RegisterType((*ListConfigsResponse)(nil), "kcs.config.v1.ListConfigsResponse")
	proto.RegisterType((*GetConfigRequest)(nil), "kcs.config.v1.GetConfigRequest")
	proto.RegisterType((*GetConfigResponse)(nil), "kcs.config.v1.GetConfigResponse")
	proto.RegisterType((*CreateConfigRequest)(nil), "kcs.config.v1.CreateConfigRequest")
	proto.RegisterType((*CreateConfigResponse)(nil), "kcs.config.v1.CreateConfigResponse")
	proto.RegisterType((*DeleteConfigRequest)(nil), "kcs.config.v1.DeleteConfigRequest")
	proto.RegisterType((*DeleteConfigResponse)(nil), "kcs.config.v1.DeleteConfigResponse")
	proto.RegisterType((*DeactivateConfigRequest)(nil), "kcs.config.v1.DeactivateConfigRequest")
	proto.RegisterType((*DeactivateConfigResponse)(nil), "kcs.config.v1.DeactivateConfigResponse")
}

func init() { proto.RegisterFile("config/v1/config_api.proto", fileDescriptor_f87cee1f80c154c0) }

var fileDescriptor_f87cee1f80c154c0 = []byte{
	// 405 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xa4, 0x54, 0xff, 0x4a, 0xc2, 0x50,
	0x18, 0x65, 0x5a, 0xd6, 0xbe, 0x69, 0xd8, 0xd5, 0x6c, 0x8c, 0xc0, 0x35, 0xa9, 0xfc, 0xa7, 0x89,
	0xf6, 0x04, 0xea, 0x48, 0xa2, 0x88, 0x58, 0x41, 0xd0, 0x3f, 0xb2, 0xd6, 0x35, 0x46, 0xb6, 0x2d,
	0xef, 0x14, 0x7a, 0x8d, 0x9e, 0xaa, 0xc7, 0x0a, 0xef, 0xbd, 0xd3, 0xfd, 0x52, 0x91, 0xfe, 0x1b,
	0x87, 0xf3, 0x9d, 0x73, 0xbe, 0x9d, 0x8f, 0x0b, 0x8a, 0xed, 0xb9, 0x23, 0xe7, 0xbd, 0x35, 0x6b,
	0xb7, 0xd8, 0xd7, 0xd0, 0xf2, 0x1d, 0xdd, 0x9f, 0x78, 0x81, 0x87, 0x4a, 0x1f, 0x36, 0xd1, 0x19,
	0xaa, 0xcf, 0xda, 0x4a, 0x2d, 0x49, 0x65, 0x34, 0xed, 0x11, 0xd0, 0x9d, 0x43, 0x82, 0x3e, 0xc5,
	0x88, 0x89, 0xbf, 0xa6, 0x98, 0x04, 0xa8, 0x0e, 0x92, 0xe7, 0x8e, 0xbf, 0x87, 0x96, 0x1d, 0x38,
	0x33, 0x2c, 0x0b, 0xaa, 0xd0, 0xdc, 0x37, 0x61, 0x0e, 0x75, 0x29, 0x82, 0x4e, 0x40, 0x74, 0xad,
	0x4f, 0x4c, 0x7c, 0xcb, 0xc6, 0x72, 0x4e, 0x15, 0x9a, 0xa2, 0xb9, 0x04, 0xb4, 0x1f, 0x01, 0x2a,
	0x31, 0x55, 0xe2, 0x7b, 0x2e, 0xc1, 0xa8, 0x01, 0x25, 0x66, 0x4e, 0x86, 0x23, 0x6f, 0xea, 0xbe,
	0x51, 0xe1, 0x5d, 0xb3, 0xc8, 0xc1, 0xeb, 0x39, 0x86, 0xce, 0xe0, 0x20, 0x24, 0x71, 0xfb, 0x1c,
	0x65, 0x85, 0xa3, 0x3c, 0x41, 0x0b, 0xf6, 0x38, 0x20, 0xe7, 0xd5, 0x7c, 0x53, 0xea, 0x1c, 0xe9,
	0xb1, 0x8d, 0x75, 0x66, 0x6e, 0x86, 0x2c, 0xcd, 0x80, 0xf2, 0x00, 0xf3, 0x48, 0xe1, 0x9e, 0x08,
	0x76, 0xe6, 0xa9, 0x69, 0x0e, 0xd1, 0xa4, 0xdf, 0x1b, 0x56, 0xeb, 0xc1, 0x61, 0x44, 0x85, 0xef,
	0x75, 0x09, 0x05, 0xe6, 0x42, 0x85, 0x56, 0x46, 0xe1, 0x24, 0xcd, 0x80, 0x4a, 0x7f, 0x82, 0xad,
	0x00, 0xc7, 0xc3, 0x6c, 0xa9, 0x52, 0x83, 0x6a, 0x5c, 0x85, 0x85, 0xd1, 0x06, 0x50, 0x31, 0xf0,
	0x18, 0x27, 0xd5, 0xb7, 0x5f, 0xb5, 0x06, 0xd5, 0xb8, 0x10, 0x37, 0xb8, 0x85, 0x63, 0x03, 0xd3,
	0x6a, 0xac, 0xff, 0x9b, 0x28, 0x20, 0xa7, 0xc5, 0x98, 0x51, 0xe7, 0x37, 0x0f, 0x22, 0x83, 0xba,
	0x0f, 0x37, 0xe8, 0x09, 0xa4, 0xc8, 0x4d, 0xa1, 0xd3, 0xc4, 0xdf, 0x49, 0x5f, 0xb1, 0xa2, 0xad,
	0xa3, 0xf0, 0xea, 0xee, 0x41, 0x5c, 0xf4, 0x89, 0xea, 0x89, 0x81, 0xe4, 0xbd, 0x28, 0xea, 0x6a,
	0x02, 0xd7, 0x7b, 0x86, 0x62, 0xb4, 0x15, 0x94, 0xcc, 0x90, 0x51, 0xbc, 0xd2, 0x58, 0xcb, 0x59,
	0x0a, 0x47, 0xdb, 0x48, 0x09, 0x67, 0x74, 0x9e, 0x12, 0xce, 0xaa, 0x13, 0xd9, 0x50, 0x4e, 0x36,
	0x80, 0xce, 0x53, 0x83, 0x99, 0x7d, 0x2b, 0x17, 0x1b, 0x79, 0xcc, 0xa4, 0x27, 0xbd, 0x88, 0x8b,
	0x07, 0xe8, 0xb5, 0x40, 0x9f, 0x9e, 0xab, 0xbf, 0x00, 0x00, 0x00, 0xff, 0xff, 0x11, 0x74, 0xad,
	0x1c, 0xbf, 0x04, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// ConfigAPIClient is the client API for ConfigAPI service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type ConfigAPIClient interface {
	ListConfigs(ctx context.Context, in *ListConfigsRequest, opts ...grpc.CallOption) (*ListConfigsResponse, error)
	GetConfig(ctx context.Context, in *GetConfigRequest, opts ...grpc.CallOption) (*GetConfigResponse, error)
	CreateConfig(ctx context.Context, in *CreateConfigRequest, opts ...grpc.CallOption) (*CreateConfigResponse, error)
	DeleteConfig(ctx context.Context, in *DeleteConfigRequest, opts ...grpc.CallOption) (*DeleteConfigResponse, error)
	DeactivateConfig(ctx context.Context, in *DeactivateConfigRequest, opts ...grpc.CallOption) (*DeactivateConfigResponse, error)
}

type configAPIClient struct {
	cc *grpc.ClientConn
}

func NewConfigAPIClient(cc *grpc.ClientConn) ConfigAPIClient {
	return &configAPIClient{cc}
}

func (c *configAPIClient) ListConfigs(ctx context.Context, in *ListConfigsRequest, opts ...grpc.CallOption) (*ListConfigsResponse, error) {
	out := new(ListConfigsResponse)
	err := c.cc.Invoke(ctx, "/kcs.config.v1.ConfigAPI/ListConfigs", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *configAPIClient) GetConfig(ctx context.Context, in *GetConfigRequest, opts ...grpc.CallOption) (*GetConfigResponse, error) {
	out := new(GetConfigResponse)
	err := c.cc.Invoke(ctx, "/kcs.config.v1.ConfigAPI/GetConfig", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *configAPIClient) CreateConfig(ctx context.Context, in *CreateConfigRequest, opts ...grpc.CallOption) (*CreateConfigResponse, error) {
	out := new(CreateConfigResponse)
	err := c.cc.Invoke(ctx, "/kcs.config.v1.ConfigAPI/CreateConfig", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *configAPIClient) DeleteConfig(ctx context.Context, in *DeleteConfigRequest, opts ...grpc.CallOption) (*DeleteConfigResponse, error) {
	out := new(DeleteConfigResponse)
	err := c.cc.Invoke(ctx, "/kcs.config.v1.ConfigAPI/DeleteConfig", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *configAPIClient) DeactivateConfig(ctx context.Context, in *DeactivateConfigRequest, opts ...grpc.CallOption) (*DeactivateConfigResponse, error) {
	out := new(DeactivateConfigResponse)
	err := c.cc.Invoke(ctx, "/kcs.config.v1.ConfigAPI/DeactivateConfig", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ConfigAPIServer is the server API for ConfigAPI service.
type ConfigAPIServer interface {
	ListConfigs(context.Context, *ListConfigsRequest) (*ListConfigsResponse, error)
	GetConfig(context.Context, *GetConfigRequest) (*GetConfigResponse, error)
	CreateConfig(context.Context, *CreateConfigRequest) (*CreateConfigResponse, error)
	DeleteConfig(context.Context, *DeleteConfigRequest) (*DeleteConfigResponse, error)
	DeactivateConfig(context.Context, *DeactivateConfigRequest) (*DeactivateConfigResponse, error)
}

// UnimplementedConfigAPIServer can be embedded to have forward compatible implementations.
type UnimplementedConfigAPIServer struct {
}

func (*UnimplementedConfigAPIServer) ListConfigs(ctx context.Context, req *ListConfigsRequest) (*ListConfigsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListConfigs not implemented")
}
func (*UnimplementedConfigAPIServer) GetConfig(ctx context.Context, req *GetConfigRequest) (*GetConfigResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetConfig not implemented")
}
func (*UnimplementedConfigAPIServer) CreateConfig(ctx context.Context, req *CreateConfigRequest) (*CreateConfigResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateConfig not implemented")
}
func (*UnimplementedConfigAPIServer) DeleteConfig(ctx context.Context, req *DeleteConfigRequest) (*DeleteConfigResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteConfig not implemented")
}
func (*UnimplementedConfigAPIServer) DeactivateConfig(ctx context.Context, req *DeactivateConfigRequest) (*DeactivateConfigResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeactivateConfig not implemented")
}

func RegisterConfigAPIServer(s *grpc.Server, srv ConfigAPIServer) {
	s.RegisterService(&_ConfigAPI_serviceDesc, srv)
}

func _ConfigAPI_ListConfigs_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListConfigsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ConfigAPIServer).ListConfigs(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/kcs.config.v1.ConfigAPI/ListConfigs",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ConfigAPIServer).ListConfigs(ctx, req.(*ListConfigsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ConfigAPI_GetConfig_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetConfigRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ConfigAPIServer).GetConfig(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/kcs.config.v1.ConfigAPI/GetConfig",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ConfigAPIServer).GetConfig(ctx, req.(*GetConfigRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ConfigAPI_CreateConfig_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateConfigRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ConfigAPIServer).CreateConfig(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/kcs.config.v1.ConfigAPI/CreateConfig",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ConfigAPIServer).CreateConfig(ctx, req.(*CreateConfigRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ConfigAPI_DeleteConfig_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteConfigRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ConfigAPIServer).DeleteConfig(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/kcs.config.v1.ConfigAPI/DeleteConfig",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ConfigAPIServer).DeleteConfig(ctx, req.(*DeleteConfigRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ConfigAPI_DeactivateConfig_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeactivateConfigRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ConfigAPIServer).DeactivateConfig(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/kcs.config.v1.ConfigAPI/DeactivateConfig",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ConfigAPIServer).DeactivateConfig(ctx, req.(*DeactivateConfigRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _ConfigAPI_serviceDesc = grpc.ServiceDesc{
	ServiceName: "kcs.config.v1.ConfigAPI",
	HandlerType: (*ConfigAPIServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ListConfigs",
			Handler:    _ConfigAPI_ListConfigs_Handler,
		},
		{
			MethodName: "GetConfig",
			Handler:    _ConfigAPI_GetConfig_Handler,
		},
		{
			MethodName: "CreateConfig",
			Handler:    _ConfigAPI_CreateConfig_Handler,
		},
		{
			MethodName: "DeleteConfig",
			Handler:    _ConfigAPI_DeleteConfig_Handler,
		},
		{
			MethodName: "DeactivateConfig",
			Handler:    _ConfigAPI_DeactivateConfig_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "config/v1/config_api.proto",
}
