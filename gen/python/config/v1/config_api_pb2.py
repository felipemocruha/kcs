# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: config/v1/config_api.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from config.v1 import config_pb2 as config_dot_v1_dot_config__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='config/v1/config_api.proto',
  package='kcs.config.v1',
  syntax='proto3',
  serialized_options=_b('Z\tconfig/v1'),
  serialized_pb=_b('\n\x1a\x63onfig/v1/config_api.proto\x12\rkcs.config.v1\x1a\x16\x63onfig/v1/config.proto\"<\n\x12ListConfigsRequest\x12\x13\n\x0bonly_active\x18\x01 \x01(\x08\x12\x11\n\tnamespace\x18\x02 \x01(\t\"l\n\x13ListConfigsResponse\x12\x15\n\rconfigs_found\x18\x01 \x01(\x05\x12\x16\n\x0e\x63onfigs_active\x18\x02 \x01(\x05\x12&\n\x07\x63onfigs\x18\x03 \x03(\x0b\x32\x15.kcs.config.v1.Config\"3\n\x10GetConfigRequest\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x11\n\tnamespace\x18\x02 \x01(\t\":\n\x11GetConfigResponse\x12%\n\x06\x63onfig\x18\x01 \x01(\x0b\x32\x15.kcs.config.v1.Config\"<\n\x13\x43reateConfigRequest\x12%\n\x06\x63onfig\x18\x01 \x01(\x0b\x32\x15.kcs.config.v1.Config\"\x16\n\x14\x43reateConfigResponse\"6\n\x13\x44\x65leteConfigRequest\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x11\n\tnamespace\x18\x02 \x01(\t\"\x16\n\x14\x44\x65leteConfigResponse\":\n\x17\x44\x65\x61\x63tivateConfigRequest\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x11\n\tnamespace\x18\x02 \x01(\t\"\x1a\n\x18\x44\x65\x61\x63tivateConfigResponse2\xc8\x03\n\tConfigAPI\x12T\n\x0bListConfigs\x12!.kcs.config.v1.ListConfigsRequest\x1a\".kcs.config.v1.ListConfigsResponse\x12N\n\tGetConfig\x12\x1f.kcs.config.v1.GetConfigRequest\x1a .kcs.config.v1.GetConfigResponse\x12W\n\x0c\x43reateConfig\x12\".kcs.config.v1.CreateConfigRequest\x1a#.kcs.config.v1.CreateConfigResponse\x12W\n\x0c\x44\x65leteConfig\x12\".kcs.config.v1.DeleteConfigRequest\x1a#.kcs.config.v1.DeleteConfigResponse\x12\x63\n\x10\x44\x65\x61\x63tivateConfig\x12&.kcs.config.v1.DeactivateConfigRequest\x1a\'.kcs.config.v1.DeactivateConfigResponseB\x0bZ\tconfig/v1b\x06proto3')
  ,
  dependencies=[config_dot_v1_dot_config__pb2.DESCRIPTOR,])




_LISTCONFIGSREQUEST = _descriptor.Descriptor(
  name='ListConfigsRequest',
  full_name='kcs.config.v1.ListConfigsRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='only_active', full_name='kcs.config.v1.ListConfigsRequest.only_active', index=0,
      number=1, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='namespace', full_name='kcs.config.v1.ListConfigsRequest.namespace', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=69,
  serialized_end=129,
)


_LISTCONFIGSRESPONSE = _descriptor.Descriptor(
  name='ListConfigsResponse',
  full_name='kcs.config.v1.ListConfigsResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='configs_found', full_name='kcs.config.v1.ListConfigsResponse.configs_found', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='configs_active', full_name='kcs.config.v1.ListConfigsResponse.configs_active', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='configs', full_name='kcs.config.v1.ListConfigsResponse.configs', index=2,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=131,
  serialized_end=239,
)


_GETCONFIGREQUEST = _descriptor.Descriptor(
  name='GetConfigRequest',
  full_name='kcs.config.v1.GetConfigRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='kcs.config.v1.GetConfigRequest.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='namespace', full_name='kcs.config.v1.GetConfigRequest.namespace', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=241,
  serialized_end=292,
)


_GETCONFIGRESPONSE = _descriptor.Descriptor(
  name='GetConfigResponse',
  full_name='kcs.config.v1.GetConfigResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='config', full_name='kcs.config.v1.GetConfigResponse.config', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=294,
  serialized_end=352,
)


_CREATECONFIGREQUEST = _descriptor.Descriptor(
  name='CreateConfigRequest',
  full_name='kcs.config.v1.CreateConfigRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='config', full_name='kcs.config.v1.CreateConfigRequest.config', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=354,
  serialized_end=414,
)


_CREATECONFIGRESPONSE = _descriptor.Descriptor(
  name='CreateConfigResponse',
  full_name='kcs.config.v1.CreateConfigResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=416,
  serialized_end=438,
)


_DELETECONFIGREQUEST = _descriptor.Descriptor(
  name='DeleteConfigRequest',
  full_name='kcs.config.v1.DeleteConfigRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='kcs.config.v1.DeleteConfigRequest.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='namespace', full_name='kcs.config.v1.DeleteConfigRequest.namespace', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=440,
  serialized_end=494,
)


_DELETECONFIGRESPONSE = _descriptor.Descriptor(
  name='DeleteConfigResponse',
  full_name='kcs.config.v1.DeleteConfigResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=496,
  serialized_end=518,
)


_DEACTIVATECONFIGREQUEST = _descriptor.Descriptor(
  name='DeactivateConfigRequest',
  full_name='kcs.config.v1.DeactivateConfigRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='kcs.config.v1.DeactivateConfigRequest.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='namespace', full_name='kcs.config.v1.DeactivateConfigRequest.namespace', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=520,
  serialized_end=578,
)


_DEACTIVATECONFIGRESPONSE = _descriptor.Descriptor(
  name='DeactivateConfigResponse',
  full_name='kcs.config.v1.DeactivateConfigResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=580,
  serialized_end=606,
)

_LISTCONFIGSRESPONSE.fields_by_name['configs'].message_type = config_dot_v1_dot_config__pb2._CONFIG
_GETCONFIGRESPONSE.fields_by_name['config'].message_type = config_dot_v1_dot_config__pb2._CONFIG
_CREATECONFIGREQUEST.fields_by_name['config'].message_type = config_dot_v1_dot_config__pb2._CONFIG
DESCRIPTOR.message_types_by_name['ListConfigsRequest'] = _LISTCONFIGSREQUEST
DESCRIPTOR.message_types_by_name['ListConfigsResponse'] = _LISTCONFIGSRESPONSE
DESCRIPTOR.message_types_by_name['GetConfigRequest'] = _GETCONFIGREQUEST
DESCRIPTOR.message_types_by_name['GetConfigResponse'] = _GETCONFIGRESPONSE
DESCRIPTOR.message_types_by_name['CreateConfigRequest'] = _CREATECONFIGREQUEST
DESCRIPTOR.message_types_by_name['CreateConfigResponse'] = _CREATECONFIGRESPONSE
DESCRIPTOR.message_types_by_name['DeleteConfigRequest'] = _DELETECONFIGREQUEST
DESCRIPTOR.message_types_by_name['DeleteConfigResponse'] = _DELETECONFIGRESPONSE
DESCRIPTOR.message_types_by_name['DeactivateConfigRequest'] = _DEACTIVATECONFIGREQUEST
DESCRIPTOR.message_types_by_name['DeactivateConfigResponse'] = _DEACTIVATECONFIGRESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ListConfigsRequest = _reflection.GeneratedProtocolMessageType('ListConfigsRequest', (_message.Message,), {
  'DESCRIPTOR' : _LISTCONFIGSREQUEST,
  '__module__' : 'config.v1.config_api_pb2'
  # @@protoc_insertion_point(class_scope:kcs.config.v1.ListConfigsRequest)
  })
_sym_db.RegisterMessage(ListConfigsRequest)

ListConfigsResponse = _reflection.GeneratedProtocolMessageType('ListConfigsResponse', (_message.Message,), {
  'DESCRIPTOR' : _LISTCONFIGSRESPONSE,
  '__module__' : 'config.v1.config_api_pb2'
  # @@protoc_insertion_point(class_scope:kcs.config.v1.ListConfigsResponse)
  })
_sym_db.RegisterMessage(ListConfigsResponse)

GetConfigRequest = _reflection.GeneratedProtocolMessageType('GetConfigRequest', (_message.Message,), {
  'DESCRIPTOR' : _GETCONFIGREQUEST,
  '__module__' : 'config.v1.config_api_pb2'
  # @@protoc_insertion_point(class_scope:kcs.config.v1.GetConfigRequest)
  })
_sym_db.RegisterMessage(GetConfigRequest)

GetConfigResponse = _reflection.GeneratedProtocolMessageType('GetConfigResponse', (_message.Message,), {
  'DESCRIPTOR' : _GETCONFIGRESPONSE,
  '__module__' : 'config.v1.config_api_pb2'
  # @@protoc_insertion_point(class_scope:kcs.config.v1.GetConfigResponse)
  })
_sym_db.RegisterMessage(GetConfigResponse)

CreateConfigRequest = _reflection.GeneratedProtocolMessageType('CreateConfigRequest', (_message.Message,), {
  'DESCRIPTOR' : _CREATECONFIGREQUEST,
  '__module__' : 'config.v1.config_api_pb2'
  # @@protoc_insertion_point(class_scope:kcs.config.v1.CreateConfigRequest)
  })
_sym_db.RegisterMessage(CreateConfigRequest)

CreateConfigResponse = _reflection.GeneratedProtocolMessageType('CreateConfigResponse', (_message.Message,), {
  'DESCRIPTOR' : _CREATECONFIGRESPONSE,
  '__module__' : 'config.v1.config_api_pb2'
  # @@protoc_insertion_point(class_scope:kcs.config.v1.CreateConfigResponse)
  })
_sym_db.RegisterMessage(CreateConfigResponse)

DeleteConfigRequest = _reflection.GeneratedProtocolMessageType('DeleteConfigRequest', (_message.Message,), {
  'DESCRIPTOR' : _DELETECONFIGREQUEST,
  '__module__' : 'config.v1.config_api_pb2'
  # @@protoc_insertion_point(class_scope:kcs.config.v1.DeleteConfigRequest)
  })
_sym_db.RegisterMessage(DeleteConfigRequest)

DeleteConfigResponse = _reflection.GeneratedProtocolMessageType('DeleteConfigResponse', (_message.Message,), {
  'DESCRIPTOR' : _DELETECONFIGRESPONSE,
  '__module__' : 'config.v1.config_api_pb2'
  # @@protoc_insertion_point(class_scope:kcs.config.v1.DeleteConfigResponse)
  })
_sym_db.RegisterMessage(DeleteConfigResponse)

DeactivateConfigRequest = _reflection.GeneratedProtocolMessageType('DeactivateConfigRequest', (_message.Message,), {
  'DESCRIPTOR' : _DEACTIVATECONFIGREQUEST,
  '__module__' : 'config.v1.config_api_pb2'
  # @@protoc_insertion_point(class_scope:kcs.config.v1.DeactivateConfigRequest)
  })
_sym_db.RegisterMessage(DeactivateConfigRequest)

DeactivateConfigResponse = _reflection.GeneratedProtocolMessageType('DeactivateConfigResponse', (_message.Message,), {
  'DESCRIPTOR' : _DEACTIVATECONFIGRESPONSE,
  '__module__' : 'config.v1.config_api_pb2'
  # @@protoc_insertion_point(class_scope:kcs.config.v1.DeactivateConfigResponse)
  })
_sym_db.RegisterMessage(DeactivateConfigResponse)


DESCRIPTOR._options = None

_CONFIGAPI = _descriptor.ServiceDescriptor(
  name='ConfigAPI',
  full_name='kcs.config.v1.ConfigAPI',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=609,
  serialized_end=1065,
  methods=[
  _descriptor.MethodDescriptor(
    name='ListConfigs',
    full_name='kcs.config.v1.ConfigAPI.ListConfigs',
    index=0,
    containing_service=None,
    input_type=_LISTCONFIGSREQUEST,
    output_type=_LISTCONFIGSRESPONSE,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetConfig',
    full_name='kcs.config.v1.ConfigAPI.GetConfig',
    index=1,
    containing_service=None,
    input_type=_GETCONFIGREQUEST,
    output_type=_GETCONFIGRESPONSE,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='CreateConfig',
    full_name='kcs.config.v1.ConfigAPI.CreateConfig',
    index=2,
    containing_service=None,
    input_type=_CREATECONFIGREQUEST,
    output_type=_CREATECONFIGRESPONSE,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='DeleteConfig',
    full_name='kcs.config.v1.ConfigAPI.DeleteConfig',
    index=3,
    containing_service=None,
    input_type=_DELETECONFIGREQUEST,
    output_type=_DELETECONFIGRESPONSE,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='DeactivateConfig',
    full_name='kcs.config.v1.ConfigAPI.DeactivateConfig',
    index=4,
    containing_service=None,
    input_type=_DEACTIVATECONFIGREQUEST,
    output_type=_DEACTIVATECONFIGRESPONSE,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_CONFIGAPI)

DESCRIPTOR.services_by_name['ConfigAPI'] = _CONFIGAPI

# @@protoc_insertion_point(module_scope)
