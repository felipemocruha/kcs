# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: config/v1/config.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='config/v1/config.proto',
  package='kcs.config.v1',
  syntax='proto3',
  serialized_options=_b('Z\tconfig/v1'),
  serialized_pb=_b('\n\x16\x63onfig/v1/config.proto\x12\rkcs.config.v1\"\x90\x02\n\x06\x43onfig\x12!\n\x04kind\x18\x01 \x01(\x0e\x32\x13.kcs.config.v1.Kind\x12\x0c\n\x04name\x18\x02 \x01(\t\x12%\n\x06\x66ormat\x18\x03 \x01(\x0e\x32\x15.kcs.config.v1.Format\x12\x11\n\tnamespace\x18\x04 \x01(\t\x12\x0f\n\x07payload\x18\x05 \x01(\x0c\x12\x34\n\x08kv_pairs\x18\x06 \x03(\x0b\x32\".kcs.config.v1.Config.KvPairsEntry\x12\x0e\n\x06\x61\x63tive\x18\x07 \x01(\x08\x12\x14\n\x0c\x61\x63tive_since\x18\x08 \x01(\x03\x1a.\n\x0cKvPairsEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t:\x02\x38\x01*D\n\x06\x46ormat\x12\x08\n\x04JSON\x10\x00\x12\x08\n\x04YAML\x10\x01\x12\x07\n\x03HCL\x10\x02\x12\x08\n\x04\x46ILE\x10\x03\x12\x0b\n\x07LITERAL\x10\x04\x12\x06\n\x02KV\x10\x05*\"\n\x04Kind\x12\x0e\n\nPLAIN_TEXT\x10\x00\x12\n\n\x06SECRET\x10\x01\x42\x0bZ\tconfig/v1b\x06proto3')
)

_FORMAT = _descriptor.EnumDescriptor(
  name='Format',
  full_name='kcs.config.v1.Format',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='JSON', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='YAML', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='HCL', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FILE', index=3, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='LITERAL', index=4, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='KV', index=5, number=5,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=316,
  serialized_end=384,
)
_sym_db.RegisterEnumDescriptor(_FORMAT)

Format = enum_type_wrapper.EnumTypeWrapper(_FORMAT)
_KIND = _descriptor.EnumDescriptor(
  name='Kind',
  full_name='kcs.config.v1.Kind',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='PLAIN_TEXT', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SECRET', index=1, number=1,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=386,
  serialized_end=420,
)
_sym_db.RegisterEnumDescriptor(_KIND)

Kind = enum_type_wrapper.EnumTypeWrapper(_KIND)
JSON = 0
YAML = 1
HCL = 2
FILE = 3
LITERAL = 4
KV = 5
PLAIN_TEXT = 0
SECRET = 1



_CONFIG_KVPAIRSENTRY = _descriptor.Descriptor(
  name='KvPairsEntry',
  full_name='kcs.config.v1.Config.KvPairsEntry',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='key', full_name='kcs.config.v1.Config.KvPairsEntry.key', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='value', full_name='kcs.config.v1.Config.KvPairsEntry.value', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=_b('8\001'),
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=268,
  serialized_end=314,
)

_CONFIG = _descriptor.Descriptor(
  name='Config',
  full_name='kcs.config.v1.Config',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='kind', full_name='kcs.config.v1.Config.kind', index=0,
      number=1, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='name', full_name='kcs.config.v1.Config.name', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='format', full_name='kcs.config.v1.Config.format', index=2,
      number=3, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='namespace', full_name='kcs.config.v1.Config.namespace', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='payload', full_name='kcs.config.v1.Config.payload', index=4,
      number=5, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='kv_pairs', full_name='kcs.config.v1.Config.kv_pairs', index=5,
      number=6, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='active', full_name='kcs.config.v1.Config.active', index=6,
      number=7, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='active_since', full_name='kcs.config.v1.Config.active_since', index=7,
      number=8, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[_CONFIG_KVPAIRSENTRY, ],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=42,
  serialized_end=314,
)

_CONFIG_KVPAIRSENTRY.containing_type = _CONFIG
_CONFIG.fields_by_name['kind'].enum_type = _KIND
_CONFIG.fields_by_name['format'].enum_type = _FORMAT
_CONFIG.fields_by_name['kv_pairs'].message_type = _CONFIG_KVPAIRSENTRY
DESCRIPTOR.message_types_by_name['Config'] = _CONFIG
DESCRIPTOR.enum_types_by_name['Format'] = _FORMAT
DESCRIPTOR.enum_types_by_name['Kind'] = _KIND
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Config = _reflection.GeneratedProtocolMessageType('Config', (_message.Message,), {

  'KvPairsEntry' : _reflection.GeneratedProtocolMessageType('KvPairsEntry', (_message.Message,), {
    'DESCRIPTOR' : _CONFIG_KVPAIRSENTRY,
    '__module__' : 'config.v1.config_pb2'
    # @@protoc_insertion_point(class_scope:kcs.config.v1.Config.KvPairsEntry)
    })
  ,
  'DESCRIPTOR' : _CONFIG,
  '__module__' : 'config.v1.config_pb2'
  # @@protoc_insertion_point(class_scope:kcs.config.v1.Config)
  })
_sym_db.RegisterMessage(Config)
_sym_db.RegisterMessage(Config.KvPairsEntry)


DESCRIPTOR._options = None
_CONFIG_KVPAIRSENTRY._options = None
# @@protoc_insertion_point(module_scope)
